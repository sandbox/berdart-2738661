/**
 * @file
 *
 */

;(function ($, Drupal) {

  Drupal.behaviors.FocusPoint = {

    attach: function (context) {

      $('.focuspoint', context).focusPoint({
        throttleDuration: 0
      });

    }

  };

})(jQuery, Drupal);
