<?php

namespace Drupal\image_focus_point\Plugin\Field\FieldFormatter;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\crop\Entity\Crop;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\image_focus_point\Plugin\Field\FieldType\ImageFocusPoint;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'focus_point_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "focus_point",
 *   label = @Translation("Focus point"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class FocusPointFormatter extends ImageFormatter {

  /**
   * Focal point configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $focalPointConfig;

  /**
   * FocusPointFormatter constructor.
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   * @param array $settings
   * @param string $label
   * @param string $view_mode
   * @param array $third_party_settings
   * @param \Drupal\Core\Session\AccountInterface $current_user
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   * @param \Drupal\Core\Config\ImmutableConfig $focal_point_config
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    AccountInterface $current_user,
    EntityStorageInterface $image_style_storage,
    ImmutableConfig $focal_point_config
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition,
      $settings, $label, $view_mode, $third_party_settings, $current_user,
      $image_style_storage);
    $this->focalPointConfig = $focal_point_config;
  }

  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity.manager')->getStorage('image_style'),
      $container->get('config.factory')->get('focal_point.settings')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $crop_type = $this->focalPointConfig->get('crop_type');

    foreach ($elements as $delta => &$element) {
      /** @var ImageItem $item */
      $item = $element['#item'];

      if ($crop = Crop::findCrop($item->entity->getFileUri(), $crop_type)) {
        $focal_point = $crop->position();
        $image_width = (int) $item->width;
        $image_height = (int) $item->height;
        $focal_point_x = ($focal_point['x'] / $image_width - .5) * 2;
        $focal_point_y = ($focal_point['y'] / $image_height - .5) * -2;

        $element['#theme'] = 'image_focus_point_formatter';
        $element['#container_attributes']['class'][] = 'focuspoint';
        $element['#container_attributes']['data-focus-x'] = $focal_point_x;
        $element['#container_attributes']['data-focus-y'] = $focal_point_y;
        $element['#container_attributes']['data-focus-w'] = $image_width;
        $element['#container_attributes']['data-focus-h'] = $image_height;

        $element['#attached']['library'][] = 'image_focus_point/focus_point.default';
      }
    }

    return $elements;
  }
}
